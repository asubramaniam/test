
from fabric.api import task

@task
def hello():
	print("hello world!")

from fabric.api import task, local
import json

@task()

def describe_instance():
	toto=json.loads(local("aws ec2 describe-instances --filters Name=key-name,Values=asub1",True))
	tab=[]
#git clone https://asubramaniam@bitbucket.org/asubramaniam/test.git
	#print (toto['Reservations'][0]['Instances'])
	for Instance in toto['Reservations'][0]['Instances']:
		tab.append(Instance['PublicDnsName'])
	print (tab)
	return tab


from fabric.api import task, settings, run, sudo, env, cd

env.key_filename='/home/annitha/.aws/asub1.pem'
env.user='ubuntu'
env.hosts=describe_instance()
@task
def deploy():
    code_dir = '/var/www/html/test'
    # check the existence of the repositiry
    with settings(warn_only=True):
        if sudo("test -d %s" % code_dir).failed:
            sudo("git clone https://asubramaniam@bitbucket.org/asubramaniam/test.git %s" % code_dir)
    # update
    with cd(code_dir):
	sudo("git fetch")
	sudo("git pull --rebase")
	
      
